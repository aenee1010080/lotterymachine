﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotteryMachine;

namespace LotteryMachine
{

    class Program
    { 
        static void Main(string[] args)
        {
            int N = 0;

            //傳入樂透球數
            Console.Write("請輸入球數：");
            N = Convert.ToInt32(Console.ReadLine());
            LotteryMachine balls = new LotteryMachine(N);

            //洗球
            balls.Shuffle();

            //選擇取球數
            Console.Write("請輸入取球數：");
            N = Convert.ToInt32(Console.ReadLine());

            //每次返回一個不重複的號碼
            for (int i = 0; i < N; i++)
                Console.WriteLine("第" + (i + 1).ToString() + "顆球:"+ balls.GetOneNumber(i).ToString());

            Console.ReadKey();
        }
    }
}
