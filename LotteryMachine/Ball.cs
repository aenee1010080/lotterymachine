﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryMachine
{
    class LotteryMachine
    {
        protected List<int> Balls = new List<int>();

        //設定球數 
        public LotteryMachine(int N)
        {
            for (int i = 1; i <= N; i++)
            {
                Balls.Add(i);
            }
        }
        
        //洗球
        public void Shuffle()
        {
            List<int> shufleball = new List<int>();
            Random r = new Random();
            int outsite;
            int output;
            int site = Balls.Count;

            for (int j = 0; j < site; j++)
            {
                outsite = r.Next(0, Balls.Count);
                shufleball.Add(Balls[outsite]);
                Balls.RemoveAt(outsite);
            }
            Balls = shufleball;
        }

        //每次返回
        public int GetOneNumber(int i)
        {
            int num = Balls[i];
            return num;
        }

    }
}
